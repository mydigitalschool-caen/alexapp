<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthorTest extends WebTestCase
{

    protected int $idAuthor = 1;

    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request('GET', '/author/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('h1', 'Author index');

    }

    public function testNew(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/author/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('h1', 'Create new Author');

        $form = $crawler->selectButton('Save')->form();
        // Remplir les champs du formulaire avec des données fictives
        $form['author[blaze]'] = 'Alexandre';

        $client->submit($form);

        $this->assertEquals(303, $client->getResponse()->getStatusCode());
        $this->assertEquals('/author/', $client->getResponse()->headers->get('Location'));

    }

    public function testUpdate(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/author/1/edit');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('h1', 'Edit Author');

        $form = $crawler->selectButton('Update')->form();
        // Remplir les champs du formulaire avec des données fictives
        $form['author[blaze]'] = 'Author Updaté';

        $client->submit($form);

        $this->assertEquals(303, $client->getResponse()->getStatusCode());
        $this->assertEquals('/author/', $client->getResponse()->headers->get('Location'));

    }

    public function testDelete(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/author/1');

        $form = $crawler->selectButton('Delete')->form();

        $client->submit($form);

        $this->assertEquals(303, $client->getResponse()->getStatusCode());
        $this->assertEquals('/author/', $client->getResponse()->headers->get('Location'));

    }
}
