<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $blaze;

    /**
     * @ORM\OneToMany(targetEntity=Joke::class, mappedBy="author")
     */
    private $jokes;

    public function __construct()
    {
        $this->jokes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlaze(): ?string
    {
        return $this->blaze;
    }

    public function setBlaze(string $blaze): self
    {
        $this->blaze = $blaze;

        return $this;
    }

    /**
     * @return Collection<int, Joke>
     */
    public function getJokes(): Collection
    {
        return $this->jokes;
    }

    public function addJoke(Joke $joke): self
    {
        if (!$this->jokes->contains($joke)) {
            $this->jokes[] = $joke;
            $joke->setAuthor($this);
        }

        return $this;
    }

    public function removeJoke(Joke $joke): self
    {
        if ($this->jokes->removeElement($joke)) {
            // set the owning side to null (unless already changed)
            if ($joke->getAuthor() === $this) {
                $joke->setAuthor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getBlaze();
    }
}
