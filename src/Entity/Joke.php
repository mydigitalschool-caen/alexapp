<?php

namespace App\Entity;

use App\Repository\JokeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JokeRepository::class)
 */
class Joke
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $punchline;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFunny;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="jokes")
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPunchline(): ?string
    {
        return $this->punchline;
    }

    public function setPunchline(string $punchline): self
    {
        $this->punchline = $punchline;

        return $this;
    }

    public function isIsFunny(): ?bool
    {
        return $this->isFunny;
    }

    public function setIsFunny(?bool $isFunny): self
    {
        $this->isFunny = $isFunny;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }
}
